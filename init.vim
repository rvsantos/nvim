" ::: Options ::: {{{
	" Display incomplete commands.
	set noshowcmd

	" Display the mode you're in.
	set showmode
	
	" Colors
	set termguicolors
	
	" Intuitive backspacing.
	set backspace=indent,eol,start
	
	" Handle multiple buffers better.
	set hidden
	
	" Enhanced command line completion.
	set wildmenu
	
	" Complete files like a shell.
	set wildmode=list:longest
	
	" Case-insensitive searching.
	set ignorecase
	
	" But case-sensitive if expression contains a capital letter.
	set smartcase
	
	" Show cursor position.
	set ruler
	
	" Highlight matches as you type.
	set incsearch
	
	" Don't highlight matches.
	set nohlsearch
	
	" Turn off line wrapping.
	set nowrap    
	
	" Show 3 lines of context around the cursor.
	set scrolloff=10
	
	" Set the terminal's title
	set title
	
	" No beeping.
	set visualbell
	
	" Don't make a backup before overwriting a file.
	set nobackup
	
	" And again.
	set nowritebackup
	set noswapfile
	set noundofile
	
	" Keep swap files in one location
	set directory=$HOME/.vim/tmp//,.
	
	" Global tab width.
	set tabstop=2
	
	" And again, related.
	set shiftwidth=2
	
	" Files open expanded
	set foldlevelstart=50
	
	" Use decent folding
	set foldmethod=indent
	
	" Show the status line all the time
	set laststatus=3
	
	set updatetime=500
	
	" Always diff using vertical mode
	set diffopt+=vertical
	
	" Automatically reads changed files
	set autoread
	
	" Open preview window below the code
	set splitbelow
	
	" Remove 'press any key to continue'
	set cmdheight=1
	
	" Enable syntax highlighting
	filetype plugin on
	syntax on
	
	" Numbers
	set nonumber
" }}}


" ::: Mappgins ::: {{{
	" ESC
	ino jk <ESC>
		
	" Indentation
	nnoremap <Tab> >>_
	nnoremap <S-Tab> <<_
	vnoremap <Tab> >gv
	vnoremap <S-Tab> <gv

	" Leader
	let mapleader=" "

	vnoremap <leader>/ :Commentary<CR>

	" fzf file fuzzy search that respects .gitignore
	" If in git directory, show only files that are committed, staged, or unstaged
	" else use regular :Files
	nnoremap <expr> <C-p> (len(system('git rev-parse')) ? ':Files' : ':GFiles --exclude-standard --others --cached')."\<cr>"
	
	" Save and Quit {{{
		nnoremap S :w<CR>
		nnoremap Q :q<CR>
		
		" Split/Moving/Resizing window
		nnoremap vv <C-W>v
		nnoremap ss <C-W>s
		
		" Move around easily
		nnoremap <C-h> <C-w>h
		nnoremap <C-j> <C-w>j
		nnoremap <C-k> <C-w>k
		nnoremap <C-l> <C-w>l
		
		" Resize with arrows
		nnoremap <silent> <Down>  :resize -2<CR>
		nnoremap <silent> <Up>  	:resize +2<CR>
		nnoremap <silent> <Left>  :vertical resize -2<CR>
		nnoremap <silent> <Right> :vertical resize +2<CR>
	" }}}
	
	" Folding with space {{{
		" za for toggle fold
		" Open all folds
		nnoremap zO zR
		" Close all folds
		nnoremap zC zM
		" Close current fold
		nnoremap zc zc
		" Close all folds except the current one
		nnoremap zf mzzMzvzz
	" }}}
" }}}


" ::: Filetype ::: {{{
	" Elixir 
	autocmd BufRead,BufNewFile *.ex set filetype=elixir
	autocmd BufRead,BufNewFile *.exs set filetype=elixir
	autocmd BufRead,BufNewFile *.sface set filetype=elixir
	autocmd BufRead,BufNewFile *.eex set filetype=elixir
	autocmd BufRead,BufNewFile *.leex set filetype=elixir
	
	" JavaScript
	autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
	autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear
	autocmd BufRead,BufNewFile *.js set filetype=javascript
	autocmd BufRead,BufNewFile *.jsx set filetype=javascriptreact
	autocmd BufRead,BufNewFile *.ts set filetype=typescript
	autocmd BufRead,BufNewFile *.tsx set filetype=typescriptreact
		
	" Sets html syntax for *.ejs files.
	autocmd BufRead,BufNewFile *.ejs set filetype=html
		
	autocmd FileType javascript set expandtab
	autocmd FileType typescript set expandtab
	
	" CSS
	autocmd BufRead,BufNewFile *.css set filetype=css
	autocmd BufRead,BufNewFile *.scss set filetype=css
	autocmd BufRead,BufNewFile *.sass set filetype=css
	autocmd FileType css set shiftwidth=2
	autocmd FileType css set tabstop=2
	autocmd FileType css set expandtab
	
	" But not for erb files...
	autocmd FileType eruby set shiftwidth=4
	autocmd FileType eruby set tabstop=4

	" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim
	" update buffer
	au CursorHold,CursorHoldI * checktime

" }}}

" ::: vim-plug ::: {{{
	let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
	if empty(glob(data_dir . '/autoload/plug.vim'))
	  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
	endif	

	call plug#begin('~/.vim/plugged')
		" Color
		Plug 'tek256/simple-dark'

		" Git
		Plug 'kdheepak/lazygit.nvim', { 'branch': 'nvim-v0.4.3' }

		" NerdTree
		Plug 'preservim/nerdtree'

		" Fzf
		Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
		Plug 'junegunn/fzf.vim'

		" Nerdtree
		Plug 'scrooloose/nerdtree'

		" which-key
		Plug 'liuchengxu/vim-which-key'

		" Lsp
		Plug 'neoclide/coc.nvim', {'branch': 'release'}

		" ...
		Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
		Plug 'pbrisbin/vim-mkdir'
		Plug 'jiangmiao/auto-pairs'
		Plug 'alvan/vim-closetag'
		Plug 'dart-lang/dart-vim-plugin'
		Plug 'adelarsq/vim-matchit'
		Plug 'tpope/vim-surround'
		Plug 'tpope/vim-commentary'
		Plug 'mattn/emmet-vim'
		Plug 'andymass/vim-matchup'
		
	call plug#end()
" }}}


" ::: Plug config ::: {{{
	" Colorscheme
	colorscheme simple-dark-transparent

	" Lazygit
	let g:lazygit_floating_window_winblend = 0 " transparency of floating window
	let g:lazygit_floating_window_scaling_factor = 0.9 " scaling factor for floating window
	let g:lazygit_floating_window_corner_chars = ['╭', '╮', '╰', '╯'] " customize lazygit popup window corner characters
	let g:lazygit_floating_window_use_plenary = 0 " use plenary.nvim to manage floating window if available
	let g:lazygit_use_neovim_remote = 1 " fallback to 0 if neovim-remote is not installed

	" Fzf
	let g:fzf_layout = { 'down': '~40%' }

	" Matchup
	let g:loaded_matchit = 1

	" Treesitter
	lua require'nvim-treesitter.configs'.setup { highlight = { enable = true } }

	" Coc {{{
		let g:coc_global_extensions = [
					\ 'coc-json', 
					\ 'coc-html', 
					\ 'coc-css',
					\ 'coc-tsserver',
					\ 'coc-emmet',
					\ 'coc-solargraph',
					\ 'coc-flutter',
					\ 'coc-cssmodules',
					\ 'coc-eslint',
					\ 'coc-diagnostic',
					\ 'coc-elixir',
					\ 'coc-erlang_ls',
					\ 'coc-graphql',
					\ 'coc-prettier',
					\ 'coc-sh',
					\ 'coc-stylelint',
					\ 'coc-tailwindcss',
					\ 'coc-vimlsp',
					\ 'coc-yaml',
					\ 'coc-highlight',
					\ 'coc-vetur'
					\ ]

		" Allow Emmet tab expansion in Insert mode
		let g:user_emmet_expandabbr_key='<Tab>'
		imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")

		" Use tab for trigger completion with characters ahead and navigate.
		" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
		" other plugin before putting this into your config.
		inoremap <silent><expr> <TAB>
					\ pumvisible() ? "\<C-n>" :
					\ <SID>check_back_space() ? "\<TAB>" :
					\ coc#refresh()
		inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

		function! s:check_back_space() abort
			let col = col('.') - 1
			return !col || getline('.')[col - 1]  =~# '\s'
		endfunction

		" Jump diagnostic
		nmap <silent> [g <Plug>(coc-diagnostic-prev)
		nmap <silent> ]g <Plug>(coc-diagnostic-next)

		" Make <CR> auto-select the first completion item and notify coc.nvim to
		" format on enter, <cr> could be remapped by other vim plugin
		inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

		" Use <c-space> to trigger completion.
		inoremap <silent><expr> <c-space> coc#refresh()

		" Highlight the symbol and its references when holding the cursor.
		autocmd CursorHold * silent call CocActionAsync('highlight')
	
		" Use K to show documentation in preview window.
		nnoremap <silent> K :call <SID>show_documentation()<CR>

		function! s:show_documentation()
			if (index(['vim','help'], &filetype) >= 0)
				execute 'h '.expand('<cword>')
			elseif (coc#rpc#ready())
				call CocActionAsync('doHover')
			else
				execute '!' . &keywordprg . " " . expand('<cword>')
			endif
		endfunction

	" }}}

	" Which-key {{{
		" Map leader to which_key
		nnoremap <silent> <leader> :silent WhichKey ','<CR>
		vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual ','<CR>
		
		let g:which_key_map =  {}
		let g:which_key_sep = ': '

		autocmd! FileType which_key
		autocmd  FileType which_key set laststatus=3 noshowmode noruler
  		\| autocmd BufLeave <buffer> set laststatus=3 showmode ruler

		" Set a shorter timeout, default is 1000
		set timeoutlen=200
		set ttimeoutlen=10
		
		let g:which_key_use_floating_win = 1
		
		" Single mappings
		let g:which_key_map['/'] = [ ':Commentary'                 	, 'comment' ]
		let g:which_key_map['f'] = [ ':Files'                           	, 'search files' ]
		let g:which_key_map['t'] = [ ':Rg'                              	, 'search text' ]
		let g:which_key_map['B'] = [ ':Buffers'                         	, 'search Buffers' ]
		let g:which_key_map['e'] = [ ':NERDTreeToggle'                   	, 'NerdTree' ]
		
		" s is for search
		let g:which_key_map.s = {
		      \ 'name' : '+search' ,
		      \ '/' : [':History/'                 , 'history'],
		      \ ';' : [':Commands'                 , 'commands'],
		      \ 'a' : [':Ag'                       , 'text Ag'],
		      \ 'B' : [':BLines'                   , 'current buffer'],
		      \ 'b' : [':Buffers'                  , 'open buffers'],
		      \ 'c' : [':Commits'                  , 'commits'],
		      \ 'C' : [':BCommits'                 , 'buffer commits'],
		      \ 'f' : [':Files'                    , 'files'],
		      \ 'G' : [':GFiles'                   , 'git files'],
		      \ 'g' : [':GFiles?'                  , 'modified git files'],
		      \ 'h' : [':History'                  , 'file history'],
		      \ 'H' : [':History:'                 , 'command history'],
		      \ 'l' : [':Lines'                    , 'lines'] ,
		      \ 'm' : [':Marks'                    , 'marks'] ,
		      \ 'M' : [':Maps'                     , 'normal maps'] ,
		      \ 'p' : [':Helptags'                 , 'help tags'] ,
		      \ 'P' : [':Tags'                     , 'project tags'],
		      \ 's' : [':CocList snippets'         , 'snippets'],
		      \ 'S' : [':Colors'                   , 'color schemes'],
		      \ 't' : [':Rg'                       , 'Rg text'],
		      \ 'T' : [':BTags'                    , 'buffer tags'],
		      \ 'w' : [':Windows'                  , 'search windows'],
		      \ 'y' : [':Filetypes'                , 'file types'],
		      \ 'z' : [':FZF'                      , 'FZF'],
		      \ }
		
		" g is for git
		let g:which_key_map.g = {
		      \ 'name' : '+git' ,
		      \ 'g' : [':LazyGit'                , 'LazyGit'],
		      \ }

		" l is for lsp
		let g:which_key_map.l = {
					\ 'name' : '+lsp',
					\ 'd' : ['<Plug>(coc-definition)' 					, 'definition'],
					\ 'y' : ['<Plug>(coc-type-definition)' 			, 'type definition'],
					\ 'r' : ['<Plug>(coc-references)' 					, 'references'],
					\ 'i' : ['<Plug>(coc-implementation)' 			, 'implementation'],
					\ 'a' : ['<Plug>(coc-codeaction-selected)' 	, 'code action'],
					\ 'n' : ['<Plug>(coc-rename)' 							, 'code action'],
					\ 'o' : [':CocList outline' 								, 'coc rename'],
					\ 'c' : [':CocList diagnostic' 							, 'coclist diagnostic'],
					\ }
	
		" Register which key map
		call which_key#register(',', "g:which_key_map")	
	" }}}
" }}}
